import React from 'react';
import 'swiper/css/swiper.css';
import './App.css';
import Posts from "./pages/home";
import { Switch, Route, Link } from 'react-router-dom'

function App() {
  return (
    <div className="App">
            <header className="App-header">
                <Link to="/" >Home</Link>
            </header>
            <Switch>
                <Route exact path="/" component={Posts} />
            </Switch>
    </div>
  );
}

export default App;
